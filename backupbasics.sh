#!  /usr/bin/env bash
#       _   ___   ___  
#__   _/ | / _ \ / _ \ 
#\ \ / / || | | | | | |
# \ V /| || |_| | |_| |
#  \_/ |_(_)___(_)___/ 
#                      
#############################
#set -x
tput civis
function spinner()
{
    local pid=$!
    local delay=0.0125
    local spinstr='|/-\'
    while [ "$(ps a | awk '{print $1}' | grep $pid)" ]; do
        local temp="${spinstr#?}"
        printf " [%c] " "$spinstr"
        local spinstr="$temp${spinstr%"$temp"}"
        sleep $delay
        printf "\b\b\b\b\b"
    done
    printf "     \b\b\b\b\b"
}

currentpath=$(pwd)
currenttape="/dev/st0"
second_IP=192.168.1.230
doneonsecond=1
LISTPATH="/mnt[1-6]/"
#############################
function exitit()
{
	echo -e "\e[042mexit\e[0m"
	tput cnorm
	#set +x
	exit $?
}
#############################
function getmounted()
{
	if [ -f /home/francois/GITLAB/dev/dev_shell_privatework/sh/getnmountall.sh ] ; then 
			sudo /home/francois/GITLAB/dev/dev_shell_privatework/sh/getnmountall.sh
	else
			exitit
	fi
}
#############################
function localdump()
{
		echo -e "\e[042mLocal pg dump\e[0m"
		pg_dump -U francois | gzip > ~/Documents/postgresql.old/pg_dump_postgres."$(date +%Y%m%d)".gz && pg_dump -Fc -d francois > ~/Documents/postgresql.old/pg_francois_postgres."$(date +%Y%m%d)"
		if [ $? -ne 0 ] ; then
			# red warnings
			echo -e "\e[91mpg backup failed"
			echo -e "continue or ctl+c\e[0m"
			read -r 
			exitit 1
		fi
}
#############################
function removallold()
{
		echo -e "\e[042mRemove old local files\e[0m"
		find ~/Documents/ -name "[GP][iI]*" -size +2G -mtime +5 -exec ls {} \; 2> /dev/null | tail -n +2 | xargs rm -f -
		find ~/Documents/postgresql.old/ -mtime +5 -exec  ls {} \; | tail -n +2 | xargs rm -f -
		echo -e "\e[042mRemove old big files on targets might be long\e[0m"
		for i in ${LISTPATH} ; do 
				find "${LISTPATH}"Documents/ -name "[GP][iI]*" -size +1G -mtime +5 -exec ls {} \; | tail -n +2 | xargs rm -f -
				find "${LISTPATH}"Documents/postgresql.old/ -mtime +5 -exec  ls {} \; | tail -n +2 | xargs rm -f -
		done
		echo -e "\e[042mold big files removed\e[0m"
}
#############################
function tarfiles()
{
		echo -e "\e[100mlocal tar files (Y/N) ? \e[0m"
		read -r  answer
		case "${answer}" in
			[Yy] | [yY][eE][sS] )
				echo -e "\e[42mremove old tarballs\e[0m"
				find ~/Documents/ -iname "GITLAB*.tar.gz" -mtime +30 -exec rm {} \; &
				find ~/Documents/ -iname "Pictures*.tar.gz" -mtime +30 -exec rm {} \; &
				echo -e "\e[42mbackup file will be big & this process will take a while : GITLAB\e[0m"
				time tar zcf "${HOME}"/Documents/GITLAB."$(date +%Y%m%d)".tar.gz --exclude-vcs "${HOME}"/GITLAB/ &
				spinner
				echo -e "\e[42mbackup here is also a quite big one : PICTURES\e[0m"
				time tar zcf "${HOME}"/Documents/Pictures."$(date +%Y%m%d)".tar.gz --exclude-vcs "${HOME}"/Pictures/ &
				spinner
			;;
			* )
				echo -e "\e[42mno local backup : ok \e[0m"
			;;
		esac
}
#############################
function coreaffect()
{
		# re-affect tasks on multi-cores to limit slow-down risks if used on old computers 0 >  1  1-4  etc 
		for i in $(ps -ef | grep -v grep | grep -i firefox | awk '{print $2}' ); do  taskset -cp 2 $i 1> /dev/null ; done &
		for i in $(ps -ef | grep -v grep | grep -i rdd | awk '{print $2}' ); do  taskset -cp 3 $i 1> /dev/null ; done &
		for i in $(ps -ef | grep -v grep | grep -i Isolated | awk '{print $2}' ); do  taskset -cp 2 $i 1> /dev/null ; done &
		for i in $(ps -ef | grep -v grep | grep -i discord | awk '{print $2}' ); do  taskset -cp 1 $i 1> /dev/null ; done &
}
#############################
function secondIP()
{
		echo -e "\e[042mChecking if $second_IP is up & available\e[0m"
		cd "${HOME}" || exit 1
		if ping -c1 $second_IP  ; then 
			echo -e "\e[042mrsync -zauv --ignore-errors ~/Pictures/ francois@${second_IP}:Pictures/\e[0m"
			time rsync -zauv --ignore-errors ~/Pictures/ ${second_IP}:Pictures/ 1> /tmp/"$(basename $0)".log 2> /tmp/"$(basename $0)".err &
			spinner
			echo -e "\e[042mrsync -zauv --ignore-errors ~/Documents/ francois@${second_IP}:Documents/\e[0m"
			time rsync -zauv --ignore-errors ~/Documents/ ${second_IP}:Documents/ 1> /tmp/"$(basename $0)".log 2> /tmp/"$(basename $0)".err &
			spinner
			echo -e "\e[042mrsync -zauv --ignore-errors ~/GITLAB/ francois@${second_IP}:GITLAB/\e[0m"
			time rsync -zauv --ignore-errors ~/GITLAB/ ${second_IP}:GITLAB/ 1> /tmp/"$(basename $0)".log 2> /tmp/"$(basename $0)".err &
			spinner
			doneonsecond=0
		else
			echo
			echo -e "\e[100msecond_IP not available for now : skipping it\e[0m"
			echo
		fi
}
#############################
function rsyncdisks()
{
		echo -e "\e[42mlocal Drives rsync to ${LISTPATH}\e[0m"
		for i in ${LISTPATH} ; do 
			for j in Documents Pictures GITLAB ; do 
				echo -e "\e[42mrsync -zauv --ignore-errors ~/$j/ $i/$j/\e[0m"
				time sudo rsync -zauv --ignore-errors ~/"$j"/ "$i"/"$j"/ 1> /tmp/"$(basename $0).${j}".2.log 2> /tmp/"$(basename $0)".1.err &
				spinner
			done
			echo
		done
		cd "$currentpath" || exit 1
}
#############################
function usbtapedrive()
{
		echo -e "\e[100mif connected :  external tape drive USB (Y/N) ? \e[0m"
		read -r  answer
		case ${answer} in
			[Yy] | [yY][eE][sS] )
				echo -e "\e[42mbackup on tape will be long : Documents\e[0m"
				echo -e "\e[42minsert tape DAT72 : Documents then press enter\e[0m"
				read -r
				sudo mt -f "${currenttape}" rewind
				time sudo tar czf "${currenttape}" --exclude-vcs "${HOME}"/Documents/ &
				spinner
				sudo mt -f "${currenttape}" rewoff
				echo -e "\e[42mbackup on tape will be long : GITLAB\e[0m"
				echo -e "\e[42minsert tape DAT72 : GITLAB then press enter\e[0m"
				read -r 
				sudo mt -f "${currenttape}" rewind
				time sudo tar czf "${currenttape}" --exclude-vcs "${HOME}"/GITLAB/ &
				spinner
				sudo mt -f "${currenttape}" rewoff
				echo -e "\e[42mbackup on tape will be long : Pictures\e[0m"
				echo -e "\e[42minsert tape DAT72 : Pictures then press enter\e[0m"
				read -r 
				sudo mt -f "${currenttape}" rewind
				time sudo tar czf "${currenttape}" --exclude-vcs "${HOME}"/Pictures/ &
				spinner
				sudo mt -f "${currenttape}" rewoff
			;;
			* )
				echo -e "\e[42mno tape backup : ok\e[0m"
			;;
		esac
}
#############################
getmounted
localdump
removallold
tarfiles
coreaffect
secondIP
rsyncdisks
usbtapedrive
echo -e "\e[33;1m\tlocal SYNC command started in background\e[0m"
sync & 
if [ $doneonsecond = 0 ] ; then 
		echo -e "\e[42mhalt second host ?\e[0m "
		read -r  answer
		case ${answer} in
			[Yy] | [yY][eE][sS] )
					ssh ${second_IP} "sudo halt -p"
			;;
			*)
					echo ok
			;;
		esac
fi
echo -e "\e[0m"
tput cnorm
exitit 0

