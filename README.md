# backup_basique

![tape](./index.png)

![LICENSE](./LICENSE)

# Feel Free

Feel free to fork this for your own needs

mine are :

- [X] Add possibility to shutdown secondary host
- [X] Documents individual tape backup (tar)
- [X] GITLAB individual tape backup (tar)
- [X] GITLAB working directory local tarball
- [X] Picture working directory local tarball
- [X] Pictures GITLAB & Documents rsync to external drives
- [X] Pictures GITLAB & Documents rsync to second host
- [X] Pictures individual tape backup (tar)
- [X] PostgreSQL dumps

# KISS

Keep it simple & stupid method is applied to this script to be easy to change for any one 

# Version

Wed Jun 28 02:21:31 AM CEST 2023

```
       _   ___   _ 
__   _/ | / _ \ / |
\ \ / / || | | || |
 \ V /| || |_| || |
  \_/ |_(_)___(_)_|
```
